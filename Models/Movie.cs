using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace RazorPagesMovie.Models{
    public class Movie{
        public int ID {get; set;}
        public string Title {get; set;}
        
        [DataType(DataType.Date)]
        [Display(Name="Release Date")]
        public DateTime ReleaseDate {get; set;}
        public string Genre {get; set;}
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Price {get; set;}
    }

    public class RazorPagesMovieContext : DbContext
    {
        public RazorPagesMovieContext (DbContextOptions<RazorPagesMovieContext> options)
            : base(options)
        {
        }

        public DbSet<RazorPagesMovie.Models.Movie> Movie { get; set; }
    }

}